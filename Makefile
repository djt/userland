INSTALL = install
YACC = yacc

PROJECT = userland.gpr

GENERATED = bin/expr/expr.c

all: prepare
	gprbuild -P $(PROJECT) -p

clean:
	gprclean -P $(PROJECT) -p
	rm -f $(GENERATED)

install: all
	gprinstall -P $(PROJECT) -p --prefix=$(if $(DESTDIR),$(DESTDIR),/)
	$(INSTALL) -Dm0755 bin/false/false.sh $(DESTDIR)/bin/false
	$(INSTALL) -Dm0755 bin/true/true.sh $(DESTDIR)/bin/true

prepare: $(GENERATED)

.y.c:
	$(YACC) -o $@ $<
