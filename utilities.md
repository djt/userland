
# Utilities

## Fixed-location

The following utilties are required by FHS to be in a particular location. Entries with a question mark are optional utilities.

```
    File        Required by         Packaged in
└┬ /bin/
 ├┬ cat         POSIX, FHS
 │├ chgrp       POSIX, FHS
 │├ chmod       POSIX, FHS
 │├ chown       POSIX, FHS
 │├ cp          POSIX, FHS
 │├ date        POSIX, FHS
 │├ dd          POSIX, FHS
 │├ df          POSIX, FHS
 │├ dmesg       FHS                 util-linux
 │├ echo        POSIX, FHS
 │├ false       POSIX, FHS
 │├ hostname    FHS                 net-tools
 │├ kill        POSIX, FHS          procps
 │├ ln          POSIX, FHS
 │├ login       FHS                 shadow
 │├ ls          POSIX, FHS
 │├ mkdir       POSIX, FHS
 │├ mknod       FHS
 │├ more        POSIX, FHS          util-linux
 │├ mount       FHS                 util-linux
 │├ mv          POSIX, FHS
 │├ ps          POSIX, FHS          procps
 │├ pwd         POSIX, FHS
 │├ rm          POSIX, FHS
 │├ rmdir       POSIX, FHS
 │├ sed         POSIX, FHS          sed
 │├ sh          POSIX, FHS          bash-binsh, dash-binsh
 │├ stty        POSIX, FHS
 │├ su          POSIX, FHS          shadow
 │├ sync        FHS
 │├ true        POSIX, FHS
 │├ umount      FHS                 util-linux
 │└ uname       POSIX, FHS
 ├ /sbin/
 ├┬ fastboot?   FHS
 │├ fasthalt?   FHS
 │├ fdisk?      FHS                 util-linux
 │├ fsck?       FHS                 util-linux
 │├ getty?      FHS
 │├ halt?       FHS                 sysvinit
 │├ ifconfig?   FHS                 net-tools
 │├ init?       FHS                 sysvinit
 │├ mkfs?       FHS                 util-linux
 │├ mkswap?     FHS                 util-linux
 │├ reboot?     FHS                 sysvinit
 │├ route?      FHS                 net-tools
 │├ shutdown    FHS                 sysvinit
 │├ swapon?     FHS                 util-linux
 │├ swapoff?    FHS                 util-linux
 │└ update?     FHS
 └┬ /usr/
  ├ /usr/bin/
  └┬ perl?      FHS                 perl
   ├ python?    FHS                 
   ├ tclsh?     FHS                 tcl
   ├ wish?      FHS                 tk
   └ expect?    FHS                 expect
```

## Somewhere in `$PATH`

The following utilities are required to be present, but not in any particular location.

Standards referenced:

* [POSIX.1-2017](https://pubs.opengroup.org/onlinepubs/9699919799/)
* [LSB 5.0](https://refspecs.linuxfoundation.org/lsb.shtml)

 Utility             | Standard          | Packaged in
---------------------+-------------------+---------------------
 `[`                 | LSB Common        |
 `admin`             | POSIX             | `heirloom-devtools`
 `alias`             | POSIX             |
 `ar`                | POSIX, LSB Common | `binutils`
 `asa`               | POSIX             |
 `at`                | POSIX, LSB Common | `at`
 `awk`               | POSIX, LSB Common | `mawk`
 `basename`          | POSIX, LSB Common |
 `batch`             | POSIX, LSB Common | `at`
 `bc`                | POSIX, LSB Common | `bc`
 `bg`                | POSIX             |
 `c99`               | POSIX             | `gcc`
 `cal`               | POSIX             | `util-linux`
 `cd`                | POSIX             | `execline`
 `cflow`             | POSIX             | `cflow`
 `chfn`              | LSB Common        | `shadow`
 `chsh`              | LSB Common        | `shadow`
 `cksum`             | POSIX, LSB Common |
 `cmp`               | POSIX, LSB Common | `diffutils`
 `col`               | LSB Common        | `util-linux`
 `comm`              | POSIX, LSB Common |
 `command`           | POSIX             |
 `compress`          | POSIX             | `ncompress`?
 `cpio`              | LSB Common        | `libarchive-tools`
 `crontab`           | POSIX, LSB Common |
 `csplit`            | POSIX, LSB Common |
 `ctags`             | POSIX             | `ctags`
 `cut`               | LSB Common        |
 `cxref`             | POSIX             |
 `delta`             | POSIX             | `heirloom-devtools`
 `diff`              | POSIX, LSB Common | `diffutils`
 `dirname`           | POSIX, LSB Common |
 `du`                | POSIX, LSB Common |
 `ed`                | POSIX, LSB Common | `ed`
 `egrep`             | LSB Common        | `grep`
 `env`               | POSIX, LSB Common |
 `ex`                | POSIX             | `vim`
 `expand`            | POSIX, LSB Common |
 `expr`              | POSIX, LSB Common |
 `fc`                | POSIX             |
 `fc-cache`          | LSB Desktop       | `fontconfig`
 `fc-list`           | LSB Desktop       | `fontconfig`
 `fc-match`          | LSB Desktop       | `fontconfig`
 `fg`                | POSIX             |
 `file`              | POSIX, LSB Common | `file`
 `find`              | POSIX, LSB Common | `findutils`
 `fold`              | POSIX, LSB Common |
 `foomatic-rip`      | LSB Imaging       | `cups-filters`
 `fort77`            | POSIX             | `gcc`?
 `fuser`             | POSIX, LSB Common | `psmisc`
 `gencat`            | POSIX, LSB Common |
 `get`               | POSIX             | `heirloom-devtools`
 `getconf`           | POSIX, LSB Common | `shimmy`
 `getopts`           | POSIX             |
 `grep`              | POSIX, LSB Common | `grep`
 `groupadd`          | LSB Common        | `shadow`
 `groupdel`          | LSB Common        | `shadow`
 `groupmod`          | LSB Common        | `shadow`
 `groups`            | LSB Common        | `shadow`
 `gs`                | LSB Imaging       | `gnu-ghostscript`
 `gzip`              | LSB Common        | `gzip`
 `gunzip`            | LSB Common        | `gzip`
 `hash`              | POSIX             |
 `head`              | POSIX, LSB Common |
 `iconv`             | POSIX, LSB Common | `musl-utils`
 `id`                | POSIX, LSB Common |
 `infocmp`           | LSB Common        | `ncurses`
 `install`           | LSB Common        |
 `install_initd`     | LSB Common        |
 `ipcrm`             | POSIX, LSB Common | `util-linux`
 `ipcs`              | POSIX, LSB Common | `util-linux`
 `jobs`              | POSIX             |
 `join`              | POSIX, LSB Common |
 `killall`           | LSB Common        | `psmisc`
 `lex`               | POSIX             | `flex`
 `link`              | POSIX             |
 `locale`            | POSIX, LSB Common | `shimmy`
 `localedef`         | POSIX, LSB Common | `shimmy`
 `logger`            | POSIX, LSB Common | `util-linux`
 `logname`           | POSIX, LSB Common |
 `lp`                | POSIX, LSB Common | `cups-client`
 `lpr`               | LSB Common        | `cups-client`
 `lsb_release`       | LSB Common        |
 `m4`                | POSIX, LSB Common | `m4`
 `mailx`             | POSIX, LSB Common | `mailx`
 `make`              | POSIX, LSB Common | `make`
 `man`               | POSIX, LSB Common | `man-db`
 `md5sum`            | LSB Common        |
 `mesg`              | POSIX             | `mesg`
 `mkfifo`            | POSIX, LSB Common |
 `mktemp`            | LSB Common        |
 `msgfmt`            | LSB Common        | `gettext-tiny`
 `newgrp`            | POSIX, LSB Common | `shadow`
 `nice`              | POSIX, LSB Common |
 `nl`                | POSIX, LSB Common |
 `nm`                | POSIX             | binutils
 `nohup`             | POSIX, LSB Common |
 `od`                | POSIX, LSB Common |
 `passwd`            | LSB Common        | `shadow`
 `paste`             | POSIX, LSB Common |
 `patch`             | POSIX, LSB Common | `patch`
 `pathchk`           | POSIX, LSB Common |
 `pax`               | POSIX, LSB Common | `heirloom-pax`
 `pidof`             | LSB Common        | `procps`
 `pr`                | POSIX, LSB Common |
 `printf`            | POSIX, LSB Common |
 `prs`               | POSIX             | `heirloom-devtools`
 `qalter`            | POSIX             |
 `qdel`              | POSIX             |
 `qhold`             | POSIX             |
 `qmove`             | POSIX             |
 `qmsg`              | POSIX             |
 `qrerun`            | POSIX             |
 `qrls`              | POSIX             |
 `qselect`           | POSIX             |
 `qsig`              | POSIX             |
 `qstat`             | POSIX             |
 `qsub`              | POSIX             |
 `read`              | POSIX             | `heirloom-devtools`
 `remove_initd`      | LSB Common        |
 `renice`            | POSIX, LSB Common | `util-linux`
 `rmdel`             | POSIX             | `heirloom-devtools`
 `sact`              | POSIX             | `heirloom-devtools`
 `sccs`              | POSIX             | `heirloom-devtools`
 `sendmail`          | LSB Common        | `ssmtp`
 `seq`               | LSB Common        |
 `sleep`             | POSIX, LSB Common |
 `sort`              | POSIX, LSB Common |
 `split`             | POSIX, LSB Common |
 `strings`           | POSIX, LSB Common | `binutils`
 `strip`             | POSIX, LSB Common | `binutils`
 `tabs`              | POSIX             | `ncurses`
 `tail`              | POSIX, LSB Common |
 `talk`              | POSIX             |
 `tar`               | LSB Common        | `libarchive-tools`
 `tee`               | POSIX, LSB Common |
 `test`              | POSIX, LSB Common |
 `tic`               | LSB Common        | `ncurses`
 `time`              | POSIX, LSB Common |
 `touch`             | POSIX, LSB Common |
 `tput`              | POSIX, LSB Common | `ncurses`
 `tr`                | POSIX, LSB Common |
 `tsort`             | POSIX, LSB Common |
 `tty`               | POSIX, LSB Common |
 `type`              | POSIX, LSB Common |
 `ulimit`            | POSIX             |
 `umask`             | POSIX             | `execline`
 `unalias`           | POSIX             |
 `uncompress`        | POSIX             | `gzip`
 `unexpand`          | POSIX, LSB Common |
 `unget`             | POSIX             | `heirloom-devtools`
 `uniq`              | POSIX, LSB Common |
 `unlink`            | POSIX             |
 `useradd`           | LSB Common        | `shadow`
 `userdel`           | LSB Common        | `shadow`
 `usermod`           | LSB Common        | `shadow`
 `uucp`              | POSIX             |
 `uudecode`          | POSIX             | `sharutils`
 `uuencode`          | POSIX             | `sharutils`
 `uustat`            | POSIX             |
 `uux`               | POSIX             |
 `val`               | POSIX             | `heirloom-devtools`
 `vi`                | POSIX             | `nvi`
 `wait`              | POSIX             | `execline`
 `wc`                | POSIX, LSB Common |
 `what`              | POSIX             |
 `who`               | POSIX             |
 `write`             | POSIX             |
 `xargs`             | POSIX, LSB Common | `findutils`
 `xdg-desktop-icon`  | LSB Desktop       | `xdg-utils`
 `xdg-desktop-menu`  | LSB Desktop       | `xdg-utils`
 `xdg-email`         | LSB Desktop       | `xdg-utils`
 `xdg-icon-resource` | LSB Desktop       | `xdg-utils`
 `xdg-mime`          | LSB Desktop       | `xdg-utils`
 `xdg-open`          | LSB Desktop       | `xdg-utils`
 `xdg-screensaver`   | LSB Desktop       | `xdg-utils`
 `yacc`              | POSIX             | `bison`, `byacc`
 `zcat`              | POSIX, LSB Common | `gzip`
