/*
 * Copyright (c) 2019 The Adélie Userland Team. All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from
 * this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 * IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <sys/utsname.h>

#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

void usage(void) __attribute__((noreturn));

int main(int argc, char **argv) {
    struct utsname sysinfo;
    bool mflag, nflag, oflag, rflag, sflag, vflag;
    mflag = nflag = oflag = rflag = sflag = vflag = false;
    char ch;
    bool space = false;

    while ((ch = getopt(argc, argv, "amnorsv")) != -1) {
        switch (ch) {
            case 'a':
                mflag = nflag = oflag = rflag = sflag = vflag = true;
                break;
            case 'm':
                mflag = true;
                break;
            case 'n':
                nflag = true;
                break;
            case 'o':
                oflag = true;
                break;
            case 'r':
                rflag = true;
                break;
            case 's':
                sflag = true;
                break;
            case 'v':
                vflag = true;
                break;
            default:
                usage();
                /*UNREACHABLE*/
        }
    }
    if (!(mflag || nflag || rflag || vflag)) {
        sflag = true;
    }

    if (uname(&sysinfo) != 0) {
        fprintf(stderr, "uname failed: %s\n", strerror(errno));
        exit(errno);
    }

    if (sflag) {
        printf("%s", sysinfo.sysname);
	space = true;
    }
    if (nflag) {
        printf("%s%s", (space ? " " : ""), sysinfo.nodename);
	space = true;
    }
    if (rflag) {
        printf("%s%s", (space ? " " : ""), sysinfo.release);
	space = true;
    }
    if (vflag) {
        printf("%s%s", (space ? " " : ""), sysinfo.version);
	space = true;
    }
    if (mflag) {
        printf("%s%s", (space ? " " : ""), sysinfo.machine);
	space = true;
    }
    if (oflag) {
        printf("%s%s", (space ? " " : ""), sysinfo.sysname);
    }
    printf("\n");

    return EXIT_SUCCESS;
}

void usage(void) {
    fprintf(stderr, "usage: uname [-amnorsv]\n");
    exit(1);
}
